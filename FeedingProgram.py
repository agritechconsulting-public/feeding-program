import os
import argparse
from utils import *
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.worksheet.datavalidation import DataValidation
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.worksheet.filters import (
    FilterColumn,
    CustomFilter,
    CustomFilters,
    DateGroupItem,
    Filters,
)
import textwrap
import inquirer


def generate_template(filename, bool_replace):
    # Check if file already exist in the current directory
    output_file = generate_output_file_string(filename, bool_replace)

    # Import styling tools
    (
        font,
        warn_font,
        greenFill,
        v_border,
        h_both_border,
        h_right_border,
    ) = create_spreadsheet_style_tools()

    # Create spreadsheet
    workbook = Workbook()
    sheet = workbook.active

    # Copy and style persistent cells, rows, and columns
    sheet["C1"] = "Author"
    sheet["C1"].font = font
    sheet["C1"].fill = greenFill
    sheet["C1"].border = h_both_border

    sheet["C2"] = "Year"
    sheet["C2"].font = font
    sheet["C2"].fill = greenFill
    sheet["C2"].border = h_both_border

    sheet["C3"] = "Diet"
    sheet["C3"].font = font
    sheet["C3"].fill = greenFill
    sheet["C3"].border = h_both_border

    sheet["C4"] = "DOI"
    sheet["C4"].font = font
    sheet["C4"].fill = greenFill
    sheet["C4"].border = h_both_border

    sheet["C5"] = "Feeding table"
    sheet["C5"].font = font
    sheet["C5"].fill = greenFill
    sheet["C5"].border = h_both_border

    sheet["A6"] = "Type"
    sheet["A6"].font = font
    sheet["A6"].fill = greenFill
    sheet["A6"].border = h_both_border

    sheet["B6"] = "Code"
    sheet["B6"].font = font
    sheet["B6"].fill = greenFill
    sheet["B6"].border = h_both_border

    sheet["C6"] = "Name"
    sheet["C6"].font = font
    sheet["C6"].fill = greenFill
    sheet["C6"].border = h_both_border

    for col in range(4, 200):
        colleter = get_column_letter(col)
        sheet[("%s6" % colleter)] = "=SUM(%s7:%s1048576)" % (colleter, colleter)

    row6 = sheet.row_dimensions[6]
    row6.fill = greenFill
    row6.border = v_border

    dv = DataValidation(type="list", formula1='"Inra-AFZ,NRC"', allow_blank=True)
    sheet.add_data_validation(dv)
    dv.add("D5:XFD5")

    col3 = sheet.column_dimensions["C"]
    col3.border = h_right_border

    # Get data from database containing nutritional values of feedstuffs and copy them to the active spreadsheet
    db_inra, db_nrc = load_data()

    for row in dataframe_to_rows(
        db_nrc[["Type", "Code", "Name"]], index=False, header=False
    ):
        sheet.append(row)

    for row in dataframe_to_rows(
        db_inra[["Type", "Code", "Name"]], index=False, header=False
    ):
        sheet.append(row)

    # Adjust column width to fit with content
    set_col_width(sheet)

    # Apply styling options
    sheet["A6"].font = font
    sheet["A6"].fill = greenFill
    sheet["A6"].border = v_border

    sheet["B6"].font = font
    sheet["B6"].fill = greenFill
    sheet["B6"].border = v_border

    sheet["C6"].font = font
    sheet["C6"].fill = greenFill
    sheet["C6"].border = v_border + h_right_border

    # Add auto-filter field
    filters = sheet.auto_filter
    filters.ref = "$A$6:$C$1048576"
    col = FilterColumn(colId=0)
    filters.filterColumn.append(col)

    # Freeze pane so user can always see feedstuffs even in large documents
    c = sheet["D7"]
    sheet.freeze_panes = c

    # Save and inform user
    try:
        workbook.save(filename=output_file)
        print_pass(("Template successfully generated to " + output_file + "!"))
    except Exception:
        print_fail(("Someting went wrong at saving time"))

    return


def generate_template_for_custom_feeds(filename, bool_replace):
    # Check if file already exist in the current directory
    output_file = generate_output_file_string(filename, bool_replace)

    db_inra, db_nrc = load_data()

    workbook = Workbook()

    workbook.create_sheet("Inra-AFZ")
    workbook.create_sheet("NRC")
    workbook.remove(workbook["Sheet"])

    export_table_headers_to_sheet(db_inra, workbook["Inra-AFZ"])
    export_table_headers_to_sheet(db_nrc, workbook["NRC"])

    # Save and inform user
    try:
        workbook.save(filename=output_file)
        print_pass(
            ("Headers for custom feeds successfully exported to " + output_file + "!")
        )
    except Exception:
        print_fail(("Someting went wrong at saving time"))

    return


def export_tables(filename, bool_replace):
    # Check if file already exist in the current directory
    output_file = generate_output_file_string(filename, bool_replace)

    db_inra, db_nrc = load_data()

    workbook = Workbook()

    workbook.create_sheet("Inra-AFZ")
    workbook.create_sheet("NRC")
    workbook.remove(workbook["Sheet"])

    export_one_table_to_sheet(db_inra, workbook["Inra-AFZ"])
    export_one_table_to_sheet(db_nrc, workbook["NRC"])

    # Save and inform user
    try:
        workbook.save(filename=output_file)
        print_pass(("Tables successfully exported to " + output_file + "!"))
    except Exception:
        print_fail(("Someting went wrong at saving time"))

    return


def add_custom_feed(input_file):
    # Get database

    input_file = input_file + ".xlsx"

    try:
        os.path.exists(input_file)
        print_info("Reading " + input_file)
        custom_feed_inra = pd.read_excel(input_file, sheet_name="Inra-AFZ")
        custom_feed_nrc = pd.read_excel(input_file, sheet_name="NRC")

        # Inra-AFZ
        custom_feed_inra["Type"] = "Custom_Inra-AFZ"
        custom_feed_inra["Code"] = "0"

        db_inra, db_nrc = load_data()
        db_inra = pd.concat([db_inra, custom_feed_inra], join="inner")
        try:
            content = (~db_inra.duplicated(subset="Name")).all()
            if content:
                save_db(db_inra, db_nrc)
                print_pass(f"Feed saved to Inra-AFZ database.")
            else:
                duplicated_names = db_inra[db_inra.duplicated(subset="Name")][
                    "Name"
                ].values
                raise ValueError(
                    f"Duplicates found for {', '.join([str(c) for c in duplicated_names])} in Inra-AFZ database, please use another name"
                )
        except ValueError as e:
            print_fail(str(e))
            print_warn("Not saving changes to Inra-AFZ database...")

        # NRC
        custom_feed_nrc["Type"] = "Custom_NRC"
        custom_feed_nrc["Code"] = "0"

        db_inra, db_nrc = load_data()

        db_nrc = pd.concat([db_nrc, custom_feed_nrc], join="inner")
        try:
            content = (~db_nrc.duplicated(subset="Name")).all()
            if content:
                save_db(db_inra, db_nrc)
                print_pass(f"Feed saved to NRC database.")
            else:
                duplicated_names = db_nrc[db_nrc.duplicated(subset="Name")][
                    "Name"
                ].values
                raise ValueError(
                    f"Duplicates found for {', '.join([str(c) for c in duplicated_names])} in NRC database, please use another name"
                )

        except ValueError as e:
            print_fail(str(e))
            print_warn("Not saving changes to NRC database...")

    except FileNotFoundError as e:
        print_fail(str(e))

    return


def delete_custom_feed(feed_name, table_name):
    db_inra, db_nrc = load_data()

    if table_name == "Inra-AFZ":
        db = db_inra
        custom_type = "Custom_Inra-AFZ"
    elif table_name == "NRC":
        db = db_nrc
        custom_type = "Custom_NRC"

    try:
        if feed_name in db[db["Type"] == custom_type]["Name"].values:
            # Execute your block of code here
            db = db.drop(db[db["Type"] == custom_type]["Name"].index)
            if table_name == "Inra-AFZ":
                save_db(db, db_nrc)
            elif table_name == "NRC":
                save_db(db_inra, db)
            print_pass(f"Feed '{feed_name}' deleted from '{table_name}'")
        else:
            # Raise an error or handle the case where the value is not found
            raise ValueError(f"Feed {feed_name} not found in '{table_name}'")
    except ValueError as e:
        print_fail(str(e))

    return


def compute(input_file, output_filename, bool_replace):
    try:
        os.path.exists(input_file)

        # Import styling tools
        (
            font,
            warn_font,
            greenFill,
            v_border,
            h_both_border,
            h_right_border,
        ) = create_spreadsheet_style_tools()

        print_info("Reading " + input_file)
        print_info("Computing...")

        # Read input data and load database
        input_file = input_file + ".xlsx"
        df = pd.read_excel(input_file, header=None)
        df = df.dropna(how="all", subset=[0, 1, 2, 3, 4, 6], axis=1)
        db_inra, db_nrc = load_data()

        # Create an instance of Diet class for each column in input file and compute accordingly
        df_tot = pd.DataFrame()
        for c in df.drop([0, 1, 2], axis=1).columns:
            result_dict = dict(
                zip(df[[2, c]].dropna().iloc[:, 0], df[[2, c]].dropna().iloc[:, 1])
            )
            d = Diet(result_dict)
            d.compute_some(db_inra, db_nrc)
            dict_from_d = [vars(d)]
            df_tot = pd.concat(
                [df_tot, pd.json_normalize(dict_from_d)], ignore_index=True
            )

        # Reorder columns for export
        first_cols = ["author", "year", "doi", "diet_name", "percentage", "table"]
        dc_columns = [col for col in df_tot.columns if col.startswith("dc.")]
        new_order = (
            first_cols
            + dc_columns
            + [col for col in df_tot.columns if col not in first_cols + dc_columns]
        )
        df_tot = df_tot[new_order]

        # Create workbook, add results, and apply styling options
        workbook = Workbook()
        sheet = workbook.active

        for row in dataframe_to_rows(df_tot, index=False, header=True):
            sheet.append(row)

        # Adjust column width to fit with content
        set_col_width(sheet)

        sheet["A1"].fill = greenFill
        sheet["B1"].fill = greenFill
        sheet["C1"].fill = greenFill
        sheet["D1"].fill = greenFill
        sheet["E1"].fill = greenFill
        sheet["F1"].fill = greenFill

        sheet["A1"].font = font
        sheet["B1"].font = font
        sheet["C1"].font = font
        sheet["D1"].font = font
        sheet["E1"].font = font
        sheet["F1"].font = font

        row1 = sheet.row_dimensions[1]
        row1.font = font

        c = sheet["G2"]
        sheet.freeze_panes = c

        # Check if file already exist in the current directory
        output_file = generate_output_file_string(output_filename, bool_replace)

        # Save and inform user
        try:
            workbook.save(filename=output_file)
            print_pass(("Results successfully saved to " + output_file + "!"))
        except Exception:
            print_fail(("Someting went wrong at saving time"))

    except FileNotFoundError as e:
        print_fail(str(e))

    return


def main():
    description = textwrap.dedent(
        """\
                    Feeding Program
                    --------------------------------
                    This program makes it possible to assess the nutritional values of a diet on the basis of the nutritional values of feed materials.
                    
                    Sauvant, D., Perez, J.-M., & Tran, G. 2004. Tables of composition and nutritional value of feed materials: Pigs, poultry, cattle, sheep, goats, rabbits, horses and fish. Paris: INRA Editions.
                    
                    National Research Council. 2012. Nutrient Requirements of Swine: Eleventh Revised Edition. Washington, DC: The National Academies Press.
                    """
    )

    example = textwrap.dedent(
        """\
                Examples
                --------------------------------
                python FeedingProgram.py --generate-template
                python FeedingProgram.py --input-file INPUT_FILE.xlsx
                python FeedingProgram.py -i ./INPUT_FILE.xlsx -o OUTPUT_FILE.xlsx
                python FeedingProgram.py --input-file ./INPUT_FILE.xlsx --output-file OUTPUT_FILE.xlsx
                python FeedingProgram.py --export-tables
                python FeedingProgram.py --add-feed-from-file FILENAME
                python FeedingProgram.py -d FEED
                python FeedingProgram.py -c
                """
    )

    parser = argparse.ArgumentParser(
        prog="python " + os.path.basename(__file__),
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=description,
        epilog=example,
    )
    parser.add_argument(
        "-g", "--generate-template", metavar="FILENAME", help="Generate a template"
    )
    parser.add_argument(
        "-c",
        "--generate-template-for-custom-feeds",
        metavar="FILENAME",
        help="Generate a template for entering new custom feeds",
    )
    parser.add_argument(
        "-a", "--add-feed-from-file", metavar="FILENAME", help="Add a feed from a file"
    )
    parser.add_argument("-d", "--delete-feed", metavar="FEED", help="Delete a feed")
    parser.add_argument(
        "-t",
        "--table",
        choices=("Inra-AFZ", "NRC"),
        required="-d" in sys.argv or "--delete-feed" in sys.argv,
    )
    parser.add_argument(
        "-e", "--export-tables", metavar="FILENAME", help="Export tables"
    )
    parser.add_argument("-i", "--input-file", help="File with diet composition to use")
    parser.add_argument("-o", "--output-file", help="Path to output file")
    parser.add_argument(
        "-r",
        "--replace",
        action=argparse.BooleanOptionalAction,
        help="Always replace existing file in case of conflict",
    )

    args = parser.parse_args()

    if args.replace:
        bool_replace = True
    else:
        bool_replace = False

    if args.generate_template:
        generate_template(args.generate_template, bool_replace)
    elif args.generate_template_for_custom_feeds:
        generate_template_for_custom_feeds(
            args.generate_template_for_custom_feeds, bool_replace
        )
    elif args.export_tables:
        export_tables(args.export_tables, bool_replace)
    elif args.add_feed_from_file:
        add_custom_feed(args.add_feed)
    elif args.delete_feed:
        delete_custom_feed(args.delete_feed, args.table)
    elif args.input_file:
        if args.output_file:
            compute(args.input_file, args.output_file, bool_replace)
        else:
            print_warn(
                "No -o or --output-file argument were passed, using default value..."
            )
            compute(args.input_file, "results", bool_replace)

    else:
        print("\n" + description)
        while True:
            print("\nSelect an action:")
            print("g) Generate template")
            print("e) Export tables")
            print("c) Generate a template for entering new custom feeds")
            print("a) Add custom feed")
            print("d) Delete custom feed")
            print("i) Calculate the nutritional values of the diet.")
            print(
                "r) Rename (default) or Replace in case of conflict with an existing file"
            )
            print("h) Display help message")
            print("x) Exit program")

            choice = input("Enter your choice (see above letters): ")

            if choice == "g":
                filename = input("Template filename (default template):")
                if filename != "":
                    pass
                else:
                    filename = "template"
                generate_template(filename, bool_replace)
            elif choice == "c":
                filename = input(
                    "Template filename for entering custom feeds (default mask):"
                )
                if filename != "":
                    pass
                else:
                    filename = "mask"
                generate_template_for_custom_feeds(filename, bool_replace)
            elif choice == "e":
                filename = input("Tables filename (default read_only_tables):")
                if filename != "":
                    pass
                else:
                    filename = "read_only_tables"
                export_tables(filename, bool_replace)
            elif choice == "a":
                filename = input("Filename containing new feed: ")
                add_custom_feed(filename)
            elif choice == "d":
                name = input("Feed name: ")
                question = [
                    inquirer.List(
                        "table",
                        message="From which table do you want to delete this feed",
                        choices=["Inra-AFZ", "NRC"],
                        carousel=True,
                    ),
                ]
                answers = inquirer.prompt(question)
                delete_custom_feed(name, answers["table"])
            elif choice == "i":
                input_filename = input("Input filename (default input):")
                if input_filename != "":
                    pass
                else:
                    input_filename = "input"
                output_filename = input("Results filename (default results):")
                if output_filename != "":
                    pass
                else:
                    output_filename = "results"
                compute(input_filename, output_filename, bool_replace)
            elif choice == "r":
                bool_replace = not bool_replace
                print_warn(
                    (
                        "In case of conflict with an existing file, the Feeding Program will now "
                        + ["replace" if bool_replace else "rename"][0]
                        + " the desired file"
                    )
                )
            elif choice == "h":
                parser.print_help()
            elif choice == "x":
                print("Exiting program...")
                break
            else:
                print_fail("Invalid choice. Please select a valid option.")

    return


if __name__ == "__main__":
    main()
    input("Press Enter to confirm exit")
