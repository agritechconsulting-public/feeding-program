#!/bin/bash

# Define the name of the virtual environment
venv_name=".venv"

# Check if the virtual environment exists
if [ ! -d "$venv_name" ]; then
    echo "Creating virtual environment: $venv_name"
    python3 -m venv "$venv_name"
    source "$venv_name/bin/activate"
    # Install packages from requirements.txt
    echo "Installing requirements packages"
    pip install -r requirements.txt
    deactivate
fi

# Activate the virtual environment
source "$venv_name/bin/activate"


# Your additional commands or code can go here
python FeedingProgram.py

# Deactivate the virtual environment
deactivate