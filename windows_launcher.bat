REM Define the name of the virtual environment
set "venv_name=.venv"

REM Check if the virtual environment exists
if not exist "%venv_name%" (
    echo Creating virtual environment: %venv_name%
    python -m venv "%venv_name%"
    REM Activate the virtual environment
    call "%venv_name%\Scripts\activate"

    REM Install packages from requirements.txt
    echo Installing requirements packages
    pip install -r requirements.txt
    REM Deactivate the virtual environment
    deactivate
)

REM Activate the virtual environment
call "%venv_name%\Scripts\activate"

REM Your additional commands or code can go here
python FeedingProgram.py

REM Deactivate the virtual environment
deactivate
