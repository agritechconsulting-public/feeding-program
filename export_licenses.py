import os
import subprocess
import json
import sys

# Create a directory for license files
licenses_dir = os.path.join("dist", "licenses")
if not os.path.exists(licenses_dir):
    os.makedirs(licenses_dir)

# Get Python license information
original_stdout = sys.stdout
with open(os.path.join(licenses_dir, "PYTHON_LICENSE.txt"), "w") as f:
    sys.stdout = f
    license.MAXLINES = 8000
    license()
    sys.stdout = original_stdout

# Get licenses information from pip-licenses
result = subprocess.run(
    [
        "pip-licenses",
        "--format=json",
        "--with-license-file",
        "-s",
        "--no-license-path",
        "--order=license",
    ],
    capture_output=True,
    text=True,
)
licenses_info = json.loads(result.stdout)

# Save each license to a separate file
for package in licenses_info:
    package_name = package["Name"]
    license_text = package["LicenseText"]
    with open(
        os.path.join(licenses_dir, f"{package_name}_LICENSE.txt"), "w", encoding="utf-8"
    ) as license_file:
        license_file.write(license_text)

print("License files have been exported in the", str(licenses_dir), "directory.")
