import sys
import os
import shutil
import numpy as np
import pandas as pd
import pickle
from utils import print_warn


def create_db(inra_filepath, nrc_filepath):
    df_inra = pd.read_csv(inra_filepath, delimiter=";", decimal=",")
    df_nrc = pd.read_csv(nrc_filepath, delimiter=";", decimal=".")

    print_warn("The default decimal is set to '.' for df nrc")

    print_warn("The default decimal is set to ',' for df inra")

    with open(os.path.join(".", "db.pkl"), "wb") as handle:
        pickle.dump([df_inra, df_nrc], handle)

    return print("Database successfully created")


create_db(os.path.join("dev_data", "inra.csv"), os.path.join("dev_data", "NRC2012.csv"))
