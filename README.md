# Feeding Program

[![Project Status: WIP – Initial development is in progress, but there has not yet been a stable, usable release suitable for the public.](https://www.repostatus.org/badges/latest/wip.svg)](https://www.repostatus.org/#wip)

This program makes it possible to assess the nutritional values of a diet on the basis of the nutritional values of feed materials. 

## References

Sauvant, D., Perez, J.-M., & Tran, G. 2004. Tables of composition and nutritional value of feed materials: Pigs, poultry, cattle, sheep, goats, rabbits, horses and fish. Paris: INRA Editions.

National Research Council. 2012. Nutrient Requirements of Swine: Eleventh Revised Edition. Washington, DC: The National Academies Press.

## Installation

### On Windows operating system

- Go to the release page and download the latest package [here](https://gitlab.com/agritechconsulting-public/feeding-program/-/releases)
- Unzip the download, go to the extracted files and double click on `FeedingProgram.exe` to launch the program

### On macOs/Linux operating system

- Need python program installed. Get a copy at https://www.python.org/downloads/. During installation, please check the box "Add Python X.X to PATH".
- Double click or execute `unix_launcher.sh`. From a fresh copy of this folder, on the first double click on this file, it will install all dependecies (python packages) that are required to run the Feeding Program. 

If macOS complains about unpermitted operation, please first run `xattr -d com.apple.quanrantine unix_launcher.sh` to allow this script being executed.

## How to Use

### First
- Double click on `unix_launcher.sh` (macOs/Linux) or `FeedingProgram.exe` (Windows) depending on your operating system.
- You first need to generate a excel template with option g).
- Specify `<your-filename-template>` or leave blank to use default.
- Press enter.
- Then open the file `<your-filename-template>`.xlsx and fill it with your data containing diet composition.
- Save it under `<your-filename-input>`.xlsx

### Then
- Go back to the feeding program and run option i). 
- Specify `<your-filename-input>` as an input.
- Specify `<your-filename-output>` or leave blank to use default.
- Press enter.
- Then open the file `<your-filename-output>`.xlsx to get your results.

Remember: you never need to add the extension .xlsx to your custom filenames. It will be added by default by the Feeding Program.

To add ingredients...

To delete ingredients...

To export feeding tables currently in use by the Feeding Program, use option e.

You can use the Feeding Program programmatically, that is non-interactively. To do so, run the following command for example in a terminal or PowerShell:

```python FeedingProgram.py --generate-template``` 

More detailed examples and options are available with :

```python FeedingProgram.py --help```


## Bundle the standalone executables from source

pyinstaller WindowsStandaloneExe.spec
