import sys
import os
import shutil
import numpy as np
import pandas as pd
import pickle
from openpyxl.styles import Font, Border, Side, PatternFill
from openpyxl.utils import get_column_letter
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.worksheet.filters import (
    FilterColumn,
    CustomFilter,
    CustomFilters,
    DateGroupItem,
    Filters,
)


def save_db(df_inra, df_nrc):
    with open(os.path.join("data", "db.pkl"), "wb") as handle:
        pickle.dump([df_inra, df_nrc], handle)

    return


def load_data():
    if os.path.isdir("data"):
        pass
    else:
        os.makedirs("data")

        # Get the path to the temporary directory
        if hasattr(sys, "_MEIPASS"):
            temp_path = sys._MEIPASS
        else:
            temp_path = os.path.dirname(os.path.abspath(__file__))

        # Copy to local
        shutil.copy(os.path.join(temp_path, "db.pkl"), "data")

    with open(os.path.join("data", "db.pkl"), "rb") as handle:
        df_inra, df_nrc = pickle.load(handle)

    return df_inra, df_nrc


@staticmethod
def print_fail(message, end="\n"):
    sys.stderr.write("\033[1;31m" + "[CRIT]\t" + message + "\x1b[0m" + end)


@staticmethod
def print_pass(message, end="\n"):
    sys.stdout.write("\033[1;32m" + "[PASS]\t" + message + "\x1b[0m" + end)


@staticmethod
def print_warn(message, end="\n"):
    sys.stderr.write("\033[1;35m" + "[WARN]\t" + message + "\x1b[0m" + end)


@staticmethod
def print_info(message, end="\n"):
    sys.stdout.write("[INFO]\t" + message + "\x1b[0m" + end)


class Diet:
    def __init__(self, dico):
        if "Author" in dico:
            self.author = dico["Author"]
            del dico["Author"]
        else:
            self.author = "None"

        if "Year" in dico:
            self.year = dico["Year"]
            del dico["Year"]
        else:
            self.year = "None"

        if "DOI" in dico:
            self.doi = dico["DOI"]
            del dico["DOI"]
        else:
            self.doi = "None"

        if "Diet" in dico:
            self.diet_name = dico["Diet"]
            del dico["Diet"]
        else:
            self.diet_name = "None"

        if "Name" in dico:
            self.percentage = dico["Name"]
            del dico["Name"]
        else:
            self.percentage = "None"

        if "Feeding table" in dico:
            self.table = dico["Feeding table"]
            del dico["Feeding table"]
        else:
            self.table = "None"

        self.dc = dico

    def compute_some(self, db_inra, db_nrc):
        if self.table == "Inra-AFZ":
            db = db_inra
            self.dry_matter = np.sum(
                [
                    (db[db["Name"] == i]["MS"].values * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.en_kcal_kg = np.sum(
                [
                    (db[db["Name"] == i]["EN Croissance"].values * j / 1000)
                    for i, j in self.dc.items()
                ]
            )
            self.en_mj_kg = np.sum(
                [
                    (
                        db[db["Name"] == i]["EN Croissance"].values
                        * j
                        * 4.18
                        / 1000
                        / 1000
                    )
                    for i, j in self.dc.items()
                ]
            )
            self.eb_kcal_kg = np.sum(
                [
                    (db[db["Name"] == i]["EB"].values * j / 1000)
                    for i, j in self.dc.items()
                ]
            )

        elif self.table == "NRC":
            db = db_nrc
            self.dry_matter = np.sum(
                [
                    (compute_or_warn(db, i, "Dry Matter") * j / 100)
                    for i, j in self.dc.items()
                ]
            )

            self.crude_fiber = np.sum(
                [
                    (compute_or_warn(db, i, "Crude fiber") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ether_extract = np.sum(
                [
                    (compute_or_warn(db, i, "Ether extract") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.acid_ether_extract = np.sum(
                [
                    (compute_or_warn(db, i, "Acid ether extract") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ash = np.sum(
                [(compute_or_warn(db, i, "Ash") * j / 100) for i, j in self.dc.items()]
            )
            self.lactose = np.sum(
                [
                    (compute_or_warn(db, i, "Lactose") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sucrose = np.sum(
                [
                    (compute_or_warn(db, i, "Sucrose") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.raffinose = np.sum(
                [
                    (compute_or_warn(db, i, "Raffinose") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.stachyose = np.sum(
                [
                    (compute_or_warn(db, i, "Stachyose") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.verbascose = np.sum(
                [
                    (compute_or_warn(db, i, "Verbascose") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.oligosaccharides = np.sum(
                [
                    (compute_or_warn(db, i, "Oligosaccharides") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.starch = np.sum(
                [
                    (compute_or_warn(db, i, "Starch") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ndf = np.sum(
                [(compute_or_warn(db, i, "NDF") * j / 100) for i, j in self.dc.items()]
            )
            self.adf = np.sum(
                [(compute_or_warn(db, i, "ADF") * j / 100) for i, j in self.dc.items()]
            )
            self.ad_lignin = np.sum(
                [
                    (compute_or_warn(db, i, "AD ligin") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_df = np.sum(
                [
                    (compute_or_warn(db, i, "Total DF") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.insol_df = np.sum(
                [
                    (compute_or_warn(db, i, "Insol DF") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sol_df = np.sum(
                [
                    (compute_or_warn(db, i, "Sol DF") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ferm_fiber = np.sum(
                [
                    (compute_or_warn(db, i, "FermFiber") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_cp = np.sum(
                [
                    (compute_or_warn(db, i, "TotalCP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.C = np.sum(
                [
                    (compute_or_warn(db, i, "Carbon") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Na = np.sum(
                [(compute_or_warn(db, i, "Na") * j / 100) for i, j in self.dc.items()]
            )
            self.Cl = np.sum(
                [(compute_or_warn(db, i, "Cl") * j / 100) for i, j in self.dc.items()]
            )
            self.K = np.sum(
                [(compute_or_warn(db, i, "K") * j / 100) for i, j in self.dc.items()]
            )
            self.Mg = np.sum(
                [(compute_or_warn(db, i, "Mg") * j / 100) for i, j in self.dc.items()]
            )
            self.GE = np.sum(
                [(compute_or_warn(db, i, "GE") * j / 100) for i, j in self.dc.items()]
            )
            self.DE = np.sum(
                [(compute_or_warn(db, i, "DE") * j / 100) for i, j in self.dc.items()]
            )
            self.ME = np.sum(
                [(compute_or_warn(db, i, "ME") * j / 100) for i, j in self.dc.items()]
            )
            self.NE = np.sum(
                [(compute_or_warn(db, i, "NE") * j / 100) for i, j in self.dc.items()]
            )

            self.total_arg = np.sum(
                [
                    (compute_or_warn(db, i, "TotalArg") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_his = np.sum(
                [
                    (compute_or_warn(db, i, "TotalHis") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_ile = np.sum(
                [
                    (compute_or_warn(db, i, "TotalIle") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_leu = np.sum(
                [
                    (compute_or_warn(db, i, "TotalLeu") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_lys = np.sum(
                [
                    (compute_or_warn(db, i, "TotalLys") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_met = np.sum(
                [
                    (compute_or_warn(db, i, "TotalMet") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_met_cys = np.sum(
                [
                    (compute_or_warn(db, i, "TotalMet + TotalCys") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_phe = np.sum(
                [
                    (compute_or_warn(db, i, "TotalPhe") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_phe_tyr = np.sum(
                [
                    (compute_or_warn(db, i, "TotalPhe + TotalTyr") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_thr = np.sum(
                [
                    (compute_or_warn(db, i, "TotalThr") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_trp = np.sum(
                [
                    (compute_or_warn(db, i, "TotalTrp") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.total_val = np.sum(
                [
                    (compute_or_warn(db, i, "TotalVal") * j / 100)
                    for i, j in self.dc.items()
                ]
            )

            self.aid_cp = np.sum(
                [
                    (compute_or_warn(db, i, "AIDCP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_arg = np.sum(
                [
                    (compute_or_warn(db, i, "AIDArg") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_his = np.sum(
                [
                    (compute_or_warn(db, i, "AidHis") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_ile = np.sum(
                [
                    (compute_or_warn(db, i, "AidIle") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_leu = np.sum(
                [
                    (compute_or_warn(db, i, "AidLeu") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_lys = np.sum(
                [
                    (compute_or_warn(db, i, "AidLys") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_met = np.sum(
                [
                    (compute_or_warn(db, i, "AidMet") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_met_cys = np.sum(
                [
                    (compute_or_warn(db, i, "AIDMetCys") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_phe = np.sum(
                [
                    (compute_or_warn(db, i, "AIDPhe") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_phe_tyr = np.sum(
                [
                    (compute_or_warn(db, i, "AIDPheTyr") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_thr = np.sum(
                [
                    (compute_or_warn(db, i, "AIDThr") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_trp = np.sum(
                [
                    (compute_or_warn(db, i, "AIDTrp") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.aid_val = np.sum(
                [
                    (compute_or_warn(db, i, "AIDVal") * j / 100)
                    for i, j in self.dc.items()
                ]
            )

            self.sid_cp = np.sum(
                [
                    (compute_or_warn(db, i, "SIDCP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_arg = np.sum(
                [
                    (compute_or_warn(db, i, "SIDArg") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_his = np.sum(
                [
                    (compute_or_warn(db, i, "SIDHis") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_ile = np.sum(
                [
                    (compute_or_warn(db, i, "SIDIle") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_leu = np.sum(
                [
                    (compute_or_warn(db, i, "SIDLeu") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_lys = np.sum(
                [
                    (compute_or_warn(db, i, "SIDLys") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_met = np.sum(
                [
                    (compute_or_warn(db, i, "SIDMet") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_met_cys = np.sum(
                [
                    (compute_or_warn(db, i, "SIDMetCys") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_phe = np.sum(
                [
                    (compute_or_warn(db, i, "SIDPhe") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_phe_tyr = np.sum(
                [
                    (compute_or_warn(db, i, "SIDPheTyr") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_thr = np.sum(
                [
                    (compute_or_warn(db, i, "SIDThr") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_trp = np.sum(
                [
                    (compute_or_warn(db, i, "SIDTrp") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sid_val = np.sum(
                [
                    (compute_or_warn(db, i, "SIDVal") * j / 100)
                    for i, j in self.dc.items()
                ]
            )

            self.total_p = np.sum(
                [
                    (compute_or_warn(db, i, "TotalPPct") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.attd_p = np.sum(
                [
                    (compute_or_warn(db, i, "ATTDP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.sttd_p = np.sum(
                [
                    (compute_or_warn(db, i, "STTDP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.phytate_p = np.sum(
                [
                    (compute_or_warn(db, i, "PhytateP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.NPPv = np.sum(
                [(compute_or_warn(db, i, "NPPv") * j / 100) for i, j in self.dc.items()]
            )
            self.NPPv_percent_total = self.NPPv / self.total_p
            self.NPPanim = np.sum(
                [
                    (compute_or_warn(db, i, "NPPanim") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.NPPanim_percent_tot = self.NPPanim / self.total_p
            self.NPPmin = np.sum(
                [
                    (compute_or_warn(db, i, "NPPmin") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.NPPmin_percent_tot = self.NPPmin / self.total_p
            self.NPPminMCP = np.sum(
                [
                    (compute_or_warn(db, i, "NPPminMCP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.NPPminDCP = np.sum(
                [
                    (compute_or_warn(db, i, "NPPminDCP") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.NPPminother = np.sum(
                [
                    (compute_or_warn(db, i, "NPPminother") * j / 100)
                    for i, j in self.dc.items()
                ]
            )

            self.Ca = np.sum(
                [(compute_or_warn(db, i, "Ca") * j / 100) for i, j in self.dc.items()]
            )
            self.Cav = np.sum(
                [(compute_or_warn(db, i, "Cav") * j / 100) for i, j in self.dc.items()]
            )
            self.Cav_percent_tot = self.Cav / self.Ca
            self.Caanim = np.sum(
                [
                    (compute_or_warn(db, i, "Caanim") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Caanim_percent = self.Caanim / self.Ca
            self.Ca_min = np.sum(
                [
                    (compute_or_warn(db, i, "Camin") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Ca_min_percent_tot = self.Ca_min / self.Ca

            self.Caveg = self.Ca - self.Ca_min - self.Caanim

            self.Ca_min_limestone = np.sum(
                [
                    (compute_or_warn(db, i, "Caminlimestone") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Ca_min_phosphate = np.sum(
                [
                    (compute_or_warn(db, i, "Caminphosphate") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ATTD_without_phytase = np.sum(
                [
                    (compute_or_warn(db, i, "ATTD without phytase") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ATTD_with_phytase = np.sum(
                [
                    (compute_or_warn(db, i, "ATTD with phytase") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.STTD_without_phytase = np.sum(
                [
                    (compute_or_warn(db, i, "STTD without phytase") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.STTD_with_phytase = np.sum(
                [
                    (compute_or_warn(db, i, "STTD with phytase") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Cu = np.sum(
                [(compute_or_warn(db, i, "Cu") * j / 100) for i, j in self.dc.items()]
            )
            self.Cuv = np.sum(
                [(compute_or_warn(db, i, "Cuv") * j / 100) for i, j in self.dc.items()]
            )
            self.Cuanim = np.sum(
                [
                    (compute_or_warn(db, i, "Cuanim") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Cumin = np.sum(
                [
                    (compute_or_warn(db, i, "Cumin") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Zn = np.sum(
                [(compute_or_warn(db, i, "Zn") * j / 100) for i, j in self.dc.items()]
            )
            self.Znv = np.sum(
                [(compute_or_warn(db, i, "Znv") * j / 100) for i, j in self.dc.items()]
            )
            self.Znanim = np.sum(
                [
                    (compute_or_warn(db, i, "Znanim") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ZnOxide = np.sum(
                [
                    (compute_or_warn(db, i, "ZnOxide") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.Znmin = np.sum(
                [
                    (compute_or_warn(db, i, "Znmin") * j / 100)
                    for i, j in self.dc.items()
                ]
            )
            self.ABC4 = np.sum(
                [(compute_or_warn(db, i, "ABC4") * j / 100) for i, j in self.dc.items()]
            )
            self.ABC3 = np.sum(
                [(compute_or_warn(db, i, "ABC3") * j / 100) for i, j in self.dc.items()]
            )

        else:
            print_fail(("Someting went wrong with the table required."))


def export_table_headers_to_sheet(db, sheet):
    # Import styling tools
    (
        font,
        warn_font,
        greenFill,
        v_border,
        h_both_border,
        h_right_border,
    ) = create_spreadsheet_style_tools()

    # Create spreadsheet
    db.drop(db.index, inplace=True)
    db.drop(columns=["Type", "Code"], inplace=True)
    for row in dataframe_to_rows(db, index=False, header=True):
        sheet.append(row)
    # Adjust column width to fit with content
    set_col_width(sheet)

    row1 = sheet.row_dimensions[1]
    row1.font = font
    row1.fill = greenFill
    row1.border = v_border

    # Freeze pane so user can always see feedstuffs even in large documents
    c = sheet["B2"]
    sheet.freeze_panes = c

    return sheet


def export_one_table_to_sheet(db, sheet):
    # Import styling tools
    (
        font,
        warn_font,
        greenFill,
        v_border,
        h_both_border,
        h_right_border,
    ) = create_spreadsheet_style_tools()

    # Warn the user that modification to this file will not affect the feeding program
    sheet[
        "A1"
    ] = "Warning: modification to this file will not affect the feeding program"
    sheet[
        "A2"
    ] = "If you wish to add or remove custom ingredients, please do so using --add-ingredient or --delete-ingredient"

    db.sort_values(by=["Type", "Code"], inplace=True)
    # Get data from database containing nutritional values of feedstuffs and copy them to the active spreadsheet
    for row in dataframe_to_rows(db, index=False, header=True):
        sheet.append(row)

    # Adjust column width to fit with content
    set_col_width(sheet)

    # Apply styling options
    sheet["A1"].font = warn_font
    sheet["A2"].font = warn_font

    sheet["A3"].font = font
    sheet["A3"].fill = greenFill
    sheet["A3"].border = v_border

    sheet["B3"].font = font
    sheet["B3"].fill = greenFill
    sheet["B3"].border = v_border

    sheet["C3"].font = font
    sheet["C3"].fill = greenFill
    sheet["C3"].border = v_border + h_right_border

    # Add auto-filter field
    filters = sheet.auto_filter
    filters.ref = "$A$3:$C$1048576"
    col = FilterColumn(colId=0)
    filters.filterColumn.append(col)

    # Freeze pane so user can always see feedstuffs even in large documents
    c = sheet["D4"]
    sheet.freeze_panes = c

    return sheet


def compute_or_warn(db, nom, val):
    if pd.isna(db[db["Name"] == nom][val].values[0]):
        print_info(("unknown " + val + " value for " + nom))
        return 0
    else:
        return db[db["Name"] == nom][val].values[0]


def set_col_width(sheet):
    for column in sheet.columns:
        max_length = 0
        column_letter = column[0].column_letter
        for cell in column:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(str(cell.value))
            except:
                pass
        adjusted_width = (max_length + 2) * 1.0  # Add some padding
        sheet.column_dimensions[column_letter].width = adjusted_width
    return


def create_spreadsheet_style_tools():
    # Define spreadsheet style
    font = Font(name="Calibri", color="000000", bold=True)
    warn_font = Font(name="Calibri", color="ff0000", bold=True)
    greenFill = PatternFill(patternType="solid", fgColor="d4ea6b")
    v_border = Border(
        top=Side(border_style="thin", color="000000"),
        bottom=Side(border_style="thin", color="000000"),
    )
    h_right_border = Border(right=Side(border_style="thin", color="000000"))
    h_both_border = Border(
        left=Side(border_style="thin", color="000000"),
        right=Side(border_style="thin", color="000000"),
    )
    return font, warn_font, greenFill, v_border, h_both_border, h_right_border


def generate_output_file_string(filename, bool_replace):
    if bool_replace & os.path.exists(filename + ".xlsx"):
        print_info(
            "A file with the requested name already exists in "
            + os.getcwd()
            + ", and will be overwritten"
        )
        new_filename = filename
        output_file = new_filename + ".xlsx"
    elif os.path.exists(filename + ".xlsx"):
        print_warn(
            "A file with the requested name already exists in "
            + os.getcwd()
            + ", and will be overwritten"
        )
        new_filename = generate_unique_filename(filename + ".xlsx")
        print_info(
            (
                "The requested file will be created under the name "
                + new_filename
                + " in "
                + os.getcwd()
            )
        )
        output_file = new_filename
    else:
        output_file = filename + ".xlsx"
        print_info("The file will be created in " + os.getcwd())
    return output_file


def generate_unique_filename(existing_filename):
    base, ext = os.path.splitext(existing_filename)
    new_filename = existing_filename
    counter = 1

    while os.path.exists(new_filename):
        new_filename = f"{base}{counter}{ext}"
        counter += 1

    return new_filename
